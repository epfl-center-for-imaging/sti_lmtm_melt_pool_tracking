import pathlib

import matplotlib.pyplot as plt
import napari
import numpy as np
import pandas as pd
import skimage


def make_annotation_layer(
    time: np.array, x: np.array, y: np.array, shape: tuple
) -> np.array:
    """
    Turns positions into binary mask layers with a cross.

    Parameters
    ----------
    time : np.array
        Frame numbers.
    x : np.array
        x positions in pixels (int).
    y : np.array
        y positions in pixels (int).
    shape : tuple
        Shape of the output array.

    Returns
    -------
    annotation_layer : np.ndarray
        Binary array of shape 'shape' with
        crosses at the specified locations.
    """
    x = np.array(x)
    y = np.array(y)
    y = np.clip(y, 0, 199)
    annotation_layer = np.zeros(shape, dtype=np.uint8)
    for i, t in enumerate(time):
        if np.isnan(y[i]) or np.isnan(x[i]):
            continue
        annotation_layer[t, int(y[i]) - 4 : int(y[i]) + 5, int(x[i])] = 255
        annotation_layer[t, int(y[i]), int(x[i]) - 4 : int(x[i]) + 5] = 255
    return annotation_layer


if __name__ == "__main__":

    PROCESSED_DATA_DIR = pathlib.Path("processed")

    for reslice_type in [
        "resliced_window_median_7t3x3y",
    ]:
        for path in (PROCESSED_DATA_DIR / "spatio_temporal_reslicing").glob(
            f"wall*_{reslice_type}.tif"
        ):
            base_name = str(path.stem).split("_resliced_window_median_7t3x3y")[0]
            print(base_name)

            positions = pd.read_csv(
                PROCESSED_DATA_DIR / f"positions/{path.stem}_positions.csv", index_col=0
            )
            positions = positions.reset_index()
            original_stack = skimage.io.imread(
                PROCESSED_DATA_DIR / f"original_data/{base_name}.tif"
            )

            mp_front_edge = make_annotation_layer(
                positions["Time frame"],
                positions["MP front edge x"],
                positions["MP front edge y"],
                original_stack.shape,
            )
            mp_back_edge = make_annotation_layer(
                positions["Time frame"],
                positions["MP back edge x"],
                positions["MP back edge y"],
                original_stack.shape,
            )
            dz_front_edge = make_annotation_layer(
                positions["Time frame"],
                positions["DZ front edge x"],
                positions["DZ front edge y"],
                original_stack.shape,
            )
            dz_back_edge = make_annotation_layer(
                positions["Time frame"],
                positions["DZ back edge x"],
                positions["DZ back edge y"],
                original_stack.shape,
            )
            mp_deepest_point = make_annotation_layer(
                positions["Time frame"],
                positions["MP deepest point x"],
                positions["MP deepest point y"],
                original_stack.shape,
            )
            dz_deepest_point = make_annotation_layer(
                positions["Time frame"],
                positions["DZ deepest point x"],
                positions["DZ deepest point y"],
                original_stack.shape,
            )
            mp_depth_ref = make_annotation_layer(
                positions["Time frame"],
                positions["MP depth ref x"],
                positions["MP depth ref y"],
                original_stack.shape,
            )
            dz_depth_ref = make_annotation_layer(
                positions["Time frame"],
                positions["DZ depth ref x"],
                positions["DZ depth ref y"],
                original_stack.shape,
            )
            viewer = napari.Viewer()
            viewer.add_image(original_stack, rgb=False)
            viewer.add_labels(mp_front_edge)
            viewer.add_labels(mp_back_edge)
            viewer.add_labels(dz_front_edge)
            viewer.add_labels(dz_back_edge)
            viewer.add_labels(mp_deepest_point)
            viewer.add_labels(dz_deepest_point)
            viewer.add_labels(mp_depth_ref)
            viewer.add_labels(dz_depth_ref)
            napari.run()

            plot_dir = PROCESSED_DATA_DIR / "plots"
            plot_dir.mkdir(exist_ok=True)

            for feature in ("MP width", "MP depth", "DZ width", "DZ depth"):
                t = positions["Time frame"]
                y = positions[feature]
                plt.plot(t, y)
                plt.title(feature)
                plt.savefig(plot_dir / f"{base_name}_{feature.replace(' ', '_')}.pdf")
                plt.close()
