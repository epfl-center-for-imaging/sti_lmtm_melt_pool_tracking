import pathlib
import warnings

import joblib
import matplotlib.pyplot as plt
import napari
import numpy as np
import pandas as pd
import scipy
import shapely.geometry
import shapely.ops
import skimage
import tqdm


def get_bounding_mask(surface_mask, positions):
    mask = ~surface_mask
    for t in range(mask.shape[0]):
        positions_t = positions.loc[positions["Resliced frame"] == t, :]
        front = positions_t.loc[positions_t["Feature"] == "Front edge", "x"]
        back = positions_t.loc[positions_t["Feature"] == "Back edge", "x"]
        depth = positions_t.loc[positions_t["Feature"] == "Depth", "y"]
        if (
            positions_t.shape[0] == 0
            or len(front) == 0
            or len(back) == 0
            or len(depth) == 0
        ):
            mask[t] = 0
        else:
            mask[t, :, : front.iloc[0]] = 0
            mask[t, :, back.iloc[0] :] = 0
            mask[t, depth.iloc[0] :] = 0
    return mask


def dz_function(x, loc, scale, height_gaussian, height_center, width_center):
    offset = np.zeros_like(x)
    offset[np.where(np.abs(x - loc) < width_center / 2)] = height_center
    gaussian = scipy.stats.norm(loc, scale).pdf(x)
    gaussian = gaussian / np.max(gaussian)
    y = height_gaussian * gaussian + offset
    points = np.array((x, y))
    line = shapely.geometry.LineString(points.transpose())
    distances = np.arange(0, line.length, 1)
    eq_points = [line.interpolate(distance) for distance in distances]
    eq_points = shapely.ops.unary_union(eq_points)
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore", category=shapely.errors.ShapelyDeprecationWarning
        )
        eq_points = np.array(eq_points).transpose()
    eq_points = eq_points[:, eq_points[1] > 1]
    return eq_points


def fit_dz(img, surface, estimate):
    x = np.arange(len(surface))
    surface = surface.astype(int)

    def obj_func(args):
        loc = args[0]
        scale = args[1]
        height_gaussian = args[2]
        height_center = args[3]
        width_center = args[4]
        points = dz_function(
            x, loc, scale, height_gaussian, height_center, width_center
        )
        points = points.astype(int)
        y = surface[points[0]] + points[1]
        val = (
            np.sum(img[(y, points[0])])
            - height_center * 0.005
            - points.shape[1] * 0.007
        )
        return val

    params = []
    for loc in np.arange(estimate[0] - 2, estimate[0] + 2):
        for scale in np.arange(estimate[1] - 2, estimate[1] + 2):
            for height_gaussian in np.arange(estimate[2] - 2, estimate[2] + 2):
                for height_center in np.arange(0, 30, 2):
                    for width_center in np.arange(
                        max(estimate[4] - 2, 5), min(estimate[4] + 2, 10)
                    ):
                        params.append(
                            (loc, scale, height_gaussian, height_center, width_center)
                        )

    res = joblib.Parallel(n_jobs=8)(joblib.delayed(obj_func)(args) for args in params)
    best_params = params[np.argmax(res)]

    fitted_points = dz_function(x, *best_params)
    fitted_points = fitted_points.astype(int)

    return fitted_points[0], surface[fitted_points[0]] + fitted_points[1], best_params


def detect_depression_zone(stack, bounding_box, surface, estimate):
    global_indices = np.where(bounding_box)
    cropped = np.zeros(stack.shape)
    edges = np.zeros(stack.shape)
    frames = np.unique(global_indices[0])
    surface_indices = np.sum(surface_mask.astype(bool), axis=1)
    df = pd.DataFrame()
    for i, frame in tqdm.tqdm(enumerate(frames)):
        indices = np.where(bounding_box[frame])
        y0 = np.min(indices[0])
        y1 = np.max(indices[0])
        x0 = np.min(indices[1])
        x1 = np.max(indices[1])
        img = stack[frame, y0:y1, x0:x1]
        # img = np.abs(skimage.filters.sobel_v(img))
        img = np.abs(skimage.filters.sobel(img))
        surface = surface_indices[frame][x0:x1] - y0
        surface = np.clip(surface, 0, img.shape[0] - 1)
        fit_x, fit_y, estimate = fit_dz(img, surface, estimate)
        fig, axes = plt.subplots(2, 1, sharex=True)
        axes[0].imshow(stack[frame, y0:y1, x0:x1], cmap="gray")
        axes[0].plot(np.arange(img.shape[1]), surface)
        axes[0].plot(fit_x, fit_y)
        axes[1].imshow(img)
        axes[1].plot(np.arange(img.shape[1]), surface)
        axes[1].plot(fit_x, fit_y)
        plt.show()
        curr_df = pd.DataFrame(
            {
                "Resliced frame": (frame,),
                "loc": (estimate[0],),
                "scale": (estimate[1],),
                "height_gaussian": (estimate[2],),
                "height_center": (estimate[3],),
                "width_center": (estimate[4],),
            }
        )
        df = pd.concat((df, curr_df))
        if i >= 30:
            break
    return df


def make_dz_layer(stack, df, surface_mask, bounding_box):
    labels = np.zeros(stack.shape, dtype=bool)
    x = np.arange(stack.shape[2])
    surface_indices = np.sum(surface_mask.astype(bool), axis=1)
    for _, row in df.iterrows():
        frame = row["Resliced frame"]
        print(frame)
        points = dz_function(
            x,
            row["loc"],
            row["scale"],
            row["height_gaussian"],
            row["height_center"],
            row["width_center"],
        )
        points = points.astype(int)
        bounding_box_offset = np.min(np.where(bounding_box[frame])[1])
        points[0] = points[0] + bounding_box_offset
        points[1] = points[1] + surface_indices[frame, points[0]]
        points = points[::-1]
        points = points.transpose()
        for point1, point2 in zip(points[:-1], points[1:]):
            rr, cc = skimage.draw.line(*point1, *point2)
            labels[frame, rr, cc] = True
    return labels


if __name__ == "__main__":

    # Directory where processed data is saved
    PROCESSED_DATA_DIR = pathlib.Path("../processed_new")
    (PROCESSED_DATA_DIR / "depression_zone").mkdir(exist_ok=True)

    for reslice_type in [
        "resliced_window_median_7t3x3y",
    ]:
        for path in (PROCESSED_DATA_DIR / "spatio_temporal_reslicing").glob(
            f"wall*_{reslice_type}.tif"
        ):
            base_name = path.stem.rstrip("_resliced_window_median_7t3x3y")
            # if base_name != "wall4_H17_0":
            # if base_name != "wall1_H5":
            if base_name != "wall4_H17_1":
                continue

            original_stack = skimage.io.imread(
                PROCESSED_DATA_DIR / f"original_data/{base_name}.tif"
            )
            surface_mask = skimage.io.imread(
                PROCESSED_DATA_DIR
                / f"edge_detection/surface/{path.stem}_surface_mask.tif",
            )

            positions = pd.read_csv(
                PROCESSED_DATA_DIR / f"positions/{path.stem}_positions.csv", index_col=0
            )

            bounding_box = get_bounding_mask(surface_mask, positions)
            skimage.io.imsave(
                PROCESSED_DATA_DIR
                / f"edge_detection/surface/{base_name}_bounding_box.tif",
                bounding_box.astype(np.uint8),
            )

            stack = skimage.io.imread(path)

            dz_df = detect_depression_zone(
                stack, bounding_box, surface_mask, estimate=(38, 10, 15, 15, 8)
            )
            dz_df.to_csv(
                PROCESSED_DATA_DIR / f"depression_zone/{path.stem}_parameters.csv"
            )

            dz_labels = make_dz_layer(stack, dz_df, surface_mask, bounding_box)
            skimage.io.imsave(
                PROCESSED_DATA_DIR / f"depression_zone/{path.stem}_mask.tif", dz_labels
            )

            viewer = napari.Viewer()
            viewer.add_image(stack)
            viewer.add_labels(dz_labels)
            napari.run()
