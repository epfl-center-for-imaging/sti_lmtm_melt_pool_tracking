On linux workstation
--------------------

1. open terminal
2. navigate to `sti_lmtm_melt_pool_tracking` using `cd`
3. Run `conda deactivate`. After running the command it should no longer say `(base)` at the beginning of the prompt.
4. Run `source venv-meltpool-p38/bin/activate`. It should now say `(venv-meltpool-p38)` at the beginning of the prompt.
5. Run `python main.py` or `jupyter notebook` depending on what kind of annotations you would like to do.
