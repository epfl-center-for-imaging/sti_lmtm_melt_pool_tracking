import collections
import pathlib
import sys
import warnings

import algotom.prep.removal
import h5py
import magicgui
import matplotlib.pyplot as plt
import napari
import numpy as np
import pandas as pd
import scipy
import skimage
import skimage.io
import skimage.segmentation
import sklearn.ensemble
import sklearn.svm


def convert_to_tif(in_path: pathlib.Path, out_path: pathlib.Path) -> None:
    """
    Converts an h5 file into a tif that can be opened with Fiji.

    Parameters
    ----------
    in_path : pathlib.Path
        Path to h5 file.
    out_path : pathlib.Path
        Path where to save tif file.

    Returns
    -------
    None
    """
    f = h5py.File(path, "r")
    skimage.io.imsave(out_path, f["exchange"]["data"])


def determine_laser_speed_and_position(
    stack: np.array, name: str = None
) -> (float, float):
    """
    Infers the laser position and speed by fitting a
    line in the spatio tempol resliced version of the
    image.

    Parameters
    ----------
    stack: np.ndarray
        The full size original images with one laser pass
        from right to left.
    name: str (optional)
        If not None, the results of the linear fit are
        save as a png file in "../processed/spatio_temporal_reslicing/{name}.png".
        Default is None.

    Returns
    -------
    coef : float
        The coefficient determining the slope of the line fitted.
    intecept : float
        The intercept of the line fitted.
    """
    resliced = np.swapaxes(stack, 0, 1)
    proj_resliced = np.max(resliced, axis=0)
    footprint = (
        np.eye(5, dtype=np.uint8)[:, ::-1]
        + np.eye(5, dtype=np.uint8, k=1)[:, ::-1]
        + np.eye(5, dtype=np.uint8, k=-1)[:, ::-1]
    )
    proj_resliced = skimage.filters.median(proj_resliced, footprint=footprint)
    proj_resliced = skimage.filters.sobel_h(proj_resliced)
    proj_resliced = proj_resliced[10:]
    maxima = proj_resliced.argmax(0) + 10
    maxima = scipy.signal.medfilt(maxima, kernel_size=9)
    x = np.arange(len(maxima))
    iso_forest = sklearn.ensemble.IsolationForest()
    inliers = iso_forest.fit_predict(np.stack((x, maxima)).transpose())
    svm = sklearn.svm.SVR(kernel="linear")
    svm.fit(x[150:-150, np.newaxis], maxima[150:-150])

    if name is not None:
        plt.imshow(proj_resliced)
        plt.scatter(x, maxima, c=inliers)
        x = np.linspace(0, len(maxima), 1000)
        plt.plot(x, svm.predict(x[:, np.newaxis]), color="red")
        plt.savefig(f"processed/spatio_temporal_reslicing/{name}.png")
        plt.close()

    intercept = svm.intercept_[0]
    coef = svm.coef_[0][0]
    return coef, intercept


def determine_laser_speed_and_position_from_points(
    point1: (float, float), point2: (float, float)
) -> (float, float):
    """
    For some experiments the automatic detection of
    laser speed and positions does not work.
    In these cases the intercept can be determined
    from two manually specified points. The points
    can be chosen from the file saved by
    'determine_laser_speed_and_position'.

    Parameters
    ----------
    point1 : (float, float)
        Coordinates of point 1.
    point2 : (float, float)
        Coordinates of point 2.

    Returns
    -------
    coef : float
        Coefficient determining the slope of the line between the points.
    intercpet : float
        Intercept of the line between the points.
    """
    coef = (point2[1] - point1[1]) / (point2[0] - point1[0])
    intercept1 = point1[1] - coef * point1[0]
    intercept2 = point2[1] - coef * point2[0]
    return coef, np.mean((intercept1, intercept2))


def spatial_temporal_reslice(
    stack: np.array, coef: float, intercept: float
) -> np.array:
    """
    Spatio temporally reslices the data to fix
    the laser in a location. The sample appears to
    move. This function uses rotation and interpoation
    that can lead to apparent distations.

    Parameters
    ----------
    stack : np.ndarray
        Full size original data.
    coef : float
        Coefficient determining the laser speed.
        Can be obtained from 'determine_laser_speed_and_position'.
    intercept : float
        Position of the laser.
        Can be obtained from 'determine_laser_speed_and_position'.

    Returns
    -------
    resliced : np.ndarray
        A resliced version of the data keeping the laser in place.
    """
    resliced = np.swapaxes(stack, 0, 1)
    theta = np.arctan(coef)
    img_height = resliced.shape[1]

    # Rotate stack to make laser position horizontal, i.e. constant in the time axis
    rot = lambda x: skimage.transform.rotate(
        x, np.rad2deg(theta), resize=True, mode="constant", cval=0
    )
    resliced = apply_2D_function_to_stack(resliced, rot)

    # Calculate postion of laser in rotated images
    l = img_height / 2
    d = intercept - l + coef * l
    laser_pos = resliced.shape[1] / 2 + np.cos(theta) * d
    laser_pos = int(laser_pos)
    resliced = resliced[:, laser_pos - 60 : laser_pos + 110, :]
    resliced = np.swapaxes(resliced, 0, 2)
    resliced = np.swapaxes(resliced, 2, 1)
    return resliced


def spatial_temporal_reslice_window(
    stack: np.array,
    coef: float,
    intercept: float,
    window_offset: int = 80,
    window_size: int = 400,
) -> (np.array, pd.DataFrame):
    """
    Spatio temporally reslices the data to fix
    the laser in a location. The sample appears to
    move. Uses a sliding window approach to make sure
    there is no distortion.

    Parameters
    ----------
    stack : np.ndarray
        Full size original data.
    coef : float
        Coefficient determining the laser speed.
        Can be obtained from 'determine_laser_speed_and_position'.
    intercept : float
        Position of the laser.
        Can be obtained from 'determine_laser_speed_and_position'.
    window_offset : int
        How far the window starts from the laser postion.
    window_size : int
        Size of the moving window.

    Returns
    -------
    resliced : np.ndarray
        A resliced version of the data keeping the laser in place.
    positions : pd.DataFrame
        A data frame containing the positions of the window and the laser
        with respect to the full size original data.
    """
    n_time_points = stack.shape[0]
    height = stack.shape[1]
    width = stack.shape[2]

    first_pos = max(0, window_offset)
    last_laser_pos = min(
        round((-intercept) / coef), width - window_size + window_offset
    )

    resliced = np.zeros(
        (last_laser_pos - first_pos, height, window_size),
        dtype=stack.dtype,
    )

    positions = pd.DataFrame(
        columns=["Time frame", "Laser position", "Window start", "Window stop"]
    )

    for i, pos in enumerate(range(first_pos, last_laser_pos)):
        t = round(intercept + pos * coef)
        resliced[i] = stack[
            t, :, pos - window_offset : pos - window_offset + window_size
        ]
        positions = pd.concat(
            (
                positions,
                pd.DataFrame(
                    {
                        "Time frame": (t,),
                        "Laser position": (pos,),
                        "Window start": (pos - window_offset,),
                        "Window stop": (pos - window_offset + window_size,),
                    }
                ),
            )
        )

    return resliced, positions


def remove_stripes(stack: np.array) -> np.array:
    """
    Removes vertical stripes in image.

    Parameters
    ----------
    stack : np.ndarray
        Size of an image.

    Returns
    -------
    stack : np.ndarray
        Image afters stripe removal.
    """
    stack = apply_2D_function_to_stack(
        stack, algotom.prep.removal.remove_stripe_based_interpolation
    )
    return stack


def estimate_material_height(stack: np.array, x: int) -> int:
    return np.sum(np.isclose(stack[:, :, x], 1), axis=1)


def apply_2D_function_to_stack(
    stack: np.array, func: collections.abc.Callable
) -> np.array:
    """
    Helper function that allows runing 2D functions on each stack.
    """
    results = []
    for i, img in enumerate(stack):
        results.append(func(img))
    stack = np.stack(tuple(results), axis=0)
    return stack


def gradient(stack: np.array, method: str = "sobel") -> np.array:
    """
    Calculates the gradient (2D) using one of the following filter:
    sobel, prewitt, scharr, farid.

    Parameters
    ----------
    stack : np.ndarray
        Data with first dim time and the remaining two space.
    method : str
        Filter to be used.

    Returns
    -------
    grad : np.ndarray
        Graident images.
    """
    if method == "sobel":
        grad = apply_2D_function_to_stack(stack, skimage.filters.sobel)
    elif method == "prewitt":
        grad = apply_2D_function_to_stack(stack, skimage.filters.prewitt)
    elif method == "scharr":
        grad = apply_2D_function_to_stack(stack, skimage.filters.scharr)
    elif method == "farid":
        grad = apply_2D_function_to_stack(stack, skimage.filters.farid)
    else:
        raise ValueError(
            f"`method` can only be 'sobel', 'prewitt', 'scharr', or 'farid', not {method}."
        )
    return grad


def radial_gradient(
    stack: np.array, center: (float, float), method: str = "sobel"
) -> (np.array, np.array):
    """
    Calculates the gradient in the raidal direction from a center.

    Parameters
    ----------
    stack : np.ndarray
        First dimension is time and the remaing two are space.
    method : str
        Filter to be used

    Returns
    -------
    rad_grad : np.ndarray
        Radial gradient images.
    angles : np.ndarray
        The direction of the gradient for each point.
    """
    if method == "sobel":
        x_grad = apply_2D_function_to_stack(stack, skimage.filters.sobel_v)
        y_grad = apply_2D_function_to_stack(stack, skimage.filters.sobel_h)
    elif method == "prewitt":
        x_grad = apply_2D_function_to_stack(stack, skimage.filters.prewitt_v)
        y_grad = apply_2D_function_to_stack(stack, skimage.filters.prewitt_h)
    elif method == "scharr":
        x_grad = apply_2D_function_to_stack(stack, skimage.filters.scharr_v)
        y_grad = apply_2D_function_to_stack(stack, skimage.filters.scharr_h)
    elif method == "farid":
        x_grad = apply_2D_function_to_stack(stack, skimage.filters.farid_v)
        y_grad = apply_2D_function_to_stack(stack, skimage.filters.farid_h)
    else:
        raise ValueError(
            f"`method` can only be 'sobel', 'prewitt', 'scharr', or 'farid', not {method}."
        )

    # Calculate radial vectors
    xx, yy = np.meshgrid(np.arange(stack.shape[2]), np.arange(stack.shape[1]))
    xx = np.tile(xx, (stack.shape[0], 1, 1))
    yy = np.tile(yy, (stack.shape[0], 1, 1))
    xx = xx - center[:, 1, np.newaxis, np.newaxis]
    yy = yy - center[:, 0, np.newaxis, np.newaxis]

    # Normalize directional vectors
    v_length = np.sqrt(xx**2 + yy**2)
    xx = xx / v_length
    yy = yy / v_length

    # Remove sign to avoid different signs infront and behind the laser
    xx = np.abs(xx)
    yy = np.abs(yy)

    # Calculate gradient magnitude
    grad_mag = np.sqrt(x_grad**2 + y_grad**2)

    # Project gradient in radial direction
    rad_grad = x_grad * xx + y_grad * yy

    return rad_grad, np.arctan2(x_grad, y_grad)


def get_surface(
    stack: np.array,
    surface_mask: np.array,
    top_offset: int = 0,
    bottom_offset: int = 20,
) -> np.array:
    """
    Extractes the pixels right below the surface of the sample.

    Parameters
    ----------
    stack : np.ndarray
        Data.
    surface_mask : np.ndarray
        A mask that is True outside of the sample.
    top_offset : int
        Number of pixels to exclude below the surface.
    bottom_offset : int
        Number of pixels to include below the surface.

    Returns
    -------
    surface : np.ndarray
        Array of height bottom_offset - top_offset.
        The other dimensions are identical to those
        of stack.
    """
    n_time_points = stack.shape[0]
    height = stack.shape[1]
    width = stack.shape[2]

    n_layers = bottom_offset - top_offset

    surface = np.zeros((n_time_points, n_layers, width), dtype=float)

    for i in range(n_time_points):
        for j in range(width):
            surface_height = np.max(np.where(surface_mask[i, :, j])[0])
            if height - surface_height - top_offset < 1:
                continue
            k = min((n_layers, height - surface_height - top_offset))
            surface[i, :k, j] = stack[
                i, surface_height + top_offset : surface_height + bottom_offset, j
            ]
    return surface


def convert_surface_labels(
    stack: np.array,
    surface_mask: np.array,
    label_layers: collections.abc.Iterable,
    label_names: collections.abc.Iterable,
    time_frames: np.array,
    line_length: int = 10,
) -> (collections.abc.Iterable, pd.DataFrame):
    """
    Converts surface labels into stack labels
    that can be displayed.

    Parameters
    ----------
    stack : np.ndarray
        Data.
    surface_mask : np.ndarray
        Mask that is true outside of the sample.
    label_layers : tuple of np.ndarrays
        Each tuple element should be a binary image.
    label_names : tuple of strs
        Names of the labeled features in the same
        order as 'label_layers'.
    time_frames : np.array
        Frame numbers corresponding the original full size
        data.
    line_length : int
        The size of the resulting cross in pixels.

    Returns
    -------
    label_stacks : tuple of np.ndarrays
        Masks contianing labels for stack.
    positions : pd.DataFrame
        Dataframe contianing the positions of the
        annotated features.
    """

    label_stacks = (np.zeros_like(stack).astype(np.uint8),) * len(label_layers)
    positions = pd.DataFrame()

    if stack.shape[0] != len(time_frames):
        raise ValueError("len(time_frames) must match the numer of frames in the stack")
    height = stack.shape[1]
    width = stack.shape[2]

    for i, t in enumerate(time_frames):
        for j, label_layer in enumerate(label_layers):
            position = np.where(label_layer[i])
            if len(position[0]) == 0:
                continue
            if not np.any(surface_mask[i, :, position]):
                continue
            surface_height = np.max(np.where(surface_mask[i, :, position[0][0]])[0])
            label_stacks[j][
                i, surface_height : surface_height + line_length, position
            ] = 255
            current_df = pd.DataFrame(
                {
                    "Time frame": (t,),
                    "Feature": (label_names[j],),
                    "x": (int(position[0][0]),),
                    "y": (int(surface_height),),
                }
            )
            positions = pd.concat((positions, current_df))
    return label_stacks, positions


def incomplete_frames(stack: np.array) -> np.array:
    """
    Returns a mask of the frames that are not complete,
    i.e. the reslicing window extends beyond the bounds
    of the full size original data.
    """
    projection = np.sum(stack, axis=1)
    mask = np.isclose(projection, 0)
    mask = np.any(mask, axis=1)
    return mask


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning
            an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")


def load_label_images(path, stack_shape):
    """
    Loads label from file and matches their size to the stack.
    If file does not exist, an empty labels array is returned.

    Parameters
    ----------
    path : str
        Path to labels tif file.
    stack_shape : tuple of ints
        Shape of the stack for which the labels are.

    Returns
    -------
    labels : np.ndarray
        Array with the labels.
    """
    try:
        labels = skimage.io.imread(path)
        height_pad = stack_shape[0] - labels.shape[0]
        width_pad = stack_shape[2] - labels.shape[1]
        labels = np.pad(labels, ((0, height_pad), (0, width_pad)))
    except FileNotFoundError:
        warnings.warn("Could not find {path} using empty labels instead.")
        labels = np.zeros(stack_shape)
    return labels


def get_depth_ref(pos, surface_mask):
    """
    Returns the referece point for a depth measurement.

    Parameters
    ----------
    pos : pd.DataFrame
        Data frame with point that need a reference.
    surface_mask : np.ndarray
        Mask delineating the surface.

    Returns
    -------
    ref_df : pd.DataFrame
        A data frame with all the reference points.
    """
    surface_indices = np.sum(surface_mask.astype(bool), axis=1)
    ref_df = pd.DataFrame()
    for _, row in pos.iterrows():
        frame = row["Resliced frame"]
        time_frame = row["Time frame"]
        x = row["x"]
        if np.isclose(x, 0):
            continue
        y_ref = surface_indices[frame, x]
        x_ref = x
        current_df = pd.DataFrame(
            {
                "Resliced frame": (frame,),
                "x": (x_ref,),
                "y": (y_ref,),
                "Time frame": (time_frame,),
            }
        )
        ref_df = pd.concat((ref_df, current_df))
    return ref_df


if __name__ == "__main__":

    # Directory with h5 files
    RAW_DATA_DIR = pathlib.Path("data")
    # Directory where processed data is saved
    PROCESSED_DATA_DIR = pathlib.Path("processed")

    ###########################
    # Conversion from h5 to tif
    ###########################
    print("Converting h5 to tif")
    (PROCESSED_DATA_DIR / "original_data").mkdir(exist_ok=True)
    for path in RAW_DATA_DIR.glob("*.h5"):
        print("\t" + path.stem)
        file_name = path.stem
        output_path = PROCESSED_DATA_DIR / f"original_data/{file_name}.tif"
        existing_output_files = (PROCESSED_DATA_DIR / "original_data").glob(
            f"{file_name}*.tif"
        )
        existing_output_files = list(existing_output_files)
        if len(existing_output_files) > 0:
            print(f"\t\tSkipping conversion of {file_name} to tif because file exists.")
            continue
        convert_to_tif(path, output_path)

    # After this step I manually split the experiments
    # with two scanning directions and flipped the ones
    # in which the laser is moving from left to right,
    # i.e. all tif files should only contain one laser
    # pass from right to left.
    query_yes_no(
        "Have you turned all images in to single laser passes from right to left?"
    )

    ###########################
    # Spatio-temporal reslicing
    ###########################

    (PROCESSED_DATA_DIR / "spatio_temporal_reslicing").mkdir(exist_ok=True)
    for path in PROCESSED_DATA_DIR.glob("original_data/wall*.tif"):
        file_name = path.stem
        out_dir = path.parent.absolute() / "../spatio_temporal_reslicing"

        print(path.stem)
        resliced_output = out_dir / (file_name + "_resliced_window_median_7t3x3y.tif")
        raw_resliced_output = out_dir / (file_name + "_raw_resliced_window.tif")
        if resliced_output.exists() and raw_resliced_output.exists():
            print("\tcontinuing because files exists")
            continue

        stripes_removed_path = pathlib.Path(f"processed/stripe_removal/{path.stem}.tif")
        if not stripes_removed_path.exists():
            print("\tstripe removal")
            stack = skimage.io.imread(path)
            stack = np.swapaxes(stack, 0, 1)
            stack = remove_stripes(stack)
            stack = np.swapaxes(stack, 0, 1)
            skimage.io.imsave(stripes_removed_path, stack)
        else:
            stack = skimage.io.imread(stripes_removed_path)

        print("\tdetermine laser speed and postion")
        # automatic detection of laser speed and position
        # does not work for all trials so some manual labels
        # are required
        manual_labels = {
            "wall4_H12": {"point1": (232, 1126), "point2": (1772, 93)},
            "wall4_H14_1": {"point1": (74, 1212), "point2": (1609, 181)},
            "wall4_H17_0": {"point1": (220, 1110), "point2": (1695, 133)},
            "wall4_H17_1": {"point1": (244, 1107), "point2": (1244, 428)},
        }

        if path.stem in manual_labels.keys():
            point1 = manual_labels[path.stem]["point1"]
            point2 = manual_labels[path.stem]["point2"]
            coef, intercept = determine_laser_speed_and_position_from_points(
                point1, point2
            )
        else:
            coef, intercept = determine_laser_speed_and_position(stack, name=path.stem)

        print("\tspatiotemporal reslicing")
        stack, positions = spatial_temporal_reslice_window(stack, coef, intercept)
        positions.to_csv(
            PROCESSED_DATA_DIR / f"spatio_temporal_reslicing/{path.stem}_positions.csv"
        )
        thresh = skimage.filters.threshold_otsu(stack)
        stack = np.clip(stack, 0, thresh)
        stack = stack / np.max(stack)
        skimage.io.imsave(
            out_dir / (file_name + "_resliced_window.tif"),
            stack,
        )
        raw_stack = skimage.io.imread(path)
        raw_stack, _ = spatial_temporal_reslice_window(raw_stack, coef, intercept)
        skimage.io.imsave(raw_resliced_output, raw_stack)

        print("\tmedian filter")
        stack = skimage.filters.median(stack, np.ones((7, 3, 3)))
        skimage.io.imsave(resliced_output, stack)

    ################
    # Edge detection
    ################
    print("Calculating gradients")
    (PROCESSED_DATA_DIR / "edge_detection").mkdir(exist_ok=True)
    for reslice_type in [
        "resliced_window_median_7t3x3y",
    ]:
        for path in (PROCESSED_DATA_DIR / "spatio_temporal_reslicing").glob(
            f"wall*_{reslice_type}.tif"
        ):
            print("\t" + path.stem)
            stack = skimage.io.imread(path)
            XPOS = 115
            material_height = estimate_material_height(stack, XPOS)
            laser_positions = np.stack(
                (material_height, np.ones(stack.shape[0]) * XPOS), axis=-1
            )
            for grad_filter in [
                "sobel",
            ]:
                output_radial_gradient = (
                    PROCESSED_DATA_DIR
                    / f"edge_detection/{path.stem}_radial_gradiet_{grad_filter}.tif"
                )
                output_gradient = (
                    PROCESSED_DATA_DIR
                    / f"edge_detection/{path.stem}_gradiet_{grad_filter}.tif"
                )
                if output_radial_gradient.exists() and output_gradient.exists():
                    print("\t\tcontinuing because files exists")
                    continue
                radial_gradient_stack, gradient_directions = radial_gradient(
                    stack, laser_positions, method=grad_filter
                )
                skimage.io.imsave(output_radial_gradient, radial_gradient_stack)
                gradient_stack = gradient(stack, method=grad_filter)
                skimage.io.imsave(output_gradient, gradient_stack)

    ################################
    # Extract edges close to surface
    ################################
    print("Extracting surface")
    (PROCESSED_DATA_DIR / "edge_detection/surface").mkdir(exist_ok=True)
    for reslice_type in [
        "resliced_window_median_7t3x3y",
    ]:
        for path in (PROCESSED_DATA_DIR / "spatio_temporal_reslicing").glob(
            f"wall*_{reslice_type}.tif"
        ):
            print("\t" + path.stem)
            for edge_type in ["magnitude", "radial"]:
                output_edge_surface = (
                    PROCESSED_DATA_DIR
                    / f"edge_detection/surface/{path.stem}_{edge_type}_edge_surface.tif"
                )
                output_surface_mask = (
                    PROCESSED_DATA_DIR
                    / f"edge_detection/surface/{path.stem}_surface_mask.tif"
                )
                if output_edge_surface.exists() and output_surface_mask.exists():
                    print("\t\tcontinuing because files exists")
                    continue
                stack = skimage.io.imread(path)
                if edge_type == "magnitude":
                    edges = skimage.io.imread(
                        PROCESSED_DATA_DIR
                        / f"edge_detection/{path.stem}_gradiet_sobel.tif"
                    )
                elif edge_type == "radial":
                    edges = skimage.io.imread(
                        PROCESSED_DATA_DIR
                        / f"edge_detection/{path.stem}_radial_gradiet_sobel.tif"
                    )
                else:
                    raise ValueError(f"Unknown edge_type {edge_type}")

                mask = incomplete_frames(stack)
                stack = stack[~mask]
                edges = edges[~mask]
                surface_mask = np.isclose(stack, 1)
                skimage.io.imsave(output_surface_mask, surface_mask)
                surface = get_surface(
                    edges[:-1], surface_mask[:-1], top_offset=0, bottom_offset=5
                )
                surface = np.sum(surface, axis=1)
                skimage.io.imsave(output_edge_surface, surface)

    ###################
    # Label edge images
    ###################
    for path in (PROCESSED_DATA_DIR / "edge_detection/surface").glob(
        f"wall*_resliced_window_median_7t3x3y_radial_edge_surface.tif"
    ):
        print(path.stem)
        front_edge_file = pathlib.Path(
            str(path).replace(".tif", "_front_edge_labels.tif")
        )
        back_edge_file = pathlib.Path(
            str(path).replace(".tif", "_back_edge_labels.tif")
        )
        dz_front_edge_file = pathlib.Path(
            str(path).replace(".tif", "_dz_front_edge_labels.tif")
        )
        dz_back_edge_file = pathlib.Path(
            str(path).replace(".tif", "_dz_back_edge_labels.tif")
        )
        if front_edge_file.exists() and back_edge_file.exists():
            print("\tannotations exist, continuing")
            continue
        viewer = napari.Viewer()
        img = skimage.io.imread(path)
        viewer.add_image(img, rgb=False)
        front_edge_shapes = viewer.add_shapes(name="Front Edge")
        back_edge_shapes = viewer.add_shapes(name="Back Edge")
        dz_front_edge_shapes = viewer.add_shapes(name="DZ front Edge")
        dz_back_edge_shapes = viewer.add_shapes(name="DZ back Edge")

        @magicgui.magicgui(call_button="Save annotations")
        def save_annotations():
            front_edge_labels = front_edge_shapes.to_labels(img.shape)
            back_edge_labels = back_edge_shapes.to_labels(img.shape)
            dz_front_edge_labels = dz_front_edge_shapes.to_labels(img.shape)
            dz_back_edge_labels = dz_back_edge_shapes.to_labels(img.shape)
            skimage.io.imsave(
                front_edge_file,
                front_edge_labels.astype(np.uint8),
            )
            skimage.io.imsave(
                back_edge_file,
                back_edge_labels.astype(np.uint8),
            )
            skimage.io.imsave(
                dz_front_edge_file,
                dz_front_edge_labels.astype(np.uint8),
            )
            skimage.io.imsave(
                dz_back_edge_file,
                dz_back_edge_labels.astype(np.uint8),
            )

        viewer.window.add_dock_widget(save_annotations)
        napari.run()

    ##################################
    # Convert annotations to positions
    ##################################
    (PROCESSED_DATA_DIR / "positions").mkdir(exist_ok=True)
    for reslice_type in [
        "resliced_window_median_7t3x3y",
    ]:
        for path in (PROCESSED_DATA_DIR / "spatio_temporal_reslicing").glob(
            f"wall*_{reslice_type}.tif"
        ):
            base_name = str(path.stem).split("_resliced_window_median_7t3x3y")[0]

            stack = skimage.io.imread(path)
            mask = incomplete_frames(stack)
            stack = stack[~mask]
            surface_mask = np.isclose(stack, 1)
            surface_mask = skimage.io.imread(
                PROCESSED_DATA_DIR
                / f"edge_detection/surface/{path.stem}_surface_mask.tif",
            )

            back_edge_labels = load_label_images(
                PROCESSED_DATA_DIR
                / f"edge_detection/surface/{path.stem}_radial_edge_surface_back_edge_labels.tif",
                stack.shape,
            )
            front_edge_labels = load_label_images(
                PROCESSED_DATA_DIR
                / f"edge_detection/surface/{path.stem}_radial_edge_surface_front_edge_labels.tif",
                stack.shape,
            )
            dz_back_edge_labels = load_label_images(
                PROCESSED_DATA_DIR
                / f"edge_detection/surface/{path.stem}_radial_edge_surface_dz_back_edge_labels.tif",
                stack.shape,
            )
            dz_front_edge_labels = load_label_images(
                PROCESSED_DATA_DIR
                / f"edge_detection/surface/{path.stem}_radial_edge_surface_dz_front_edge_labels.tif",
                stack.shape,
            )

            label_layers = (
                back_edge_labels,
                front_edge_labels,
                dz_front_edge_labels,
                dz_back_edge_labels,
            )
            positions = pd.read_csv(
                PROCESSED_DATA_DIR
                / f"spatio_temporal_reslicing/{base_name}_positions.csv",
                index_col=0,
            )
            positions["Resliced frame"] = np.arange(positions.shape[0])
            _, annotated_positions = convert_surface_labels(
                stack,
                surface_mask,
                label_layers,
                time_frames=positions["Time frame"].to_numpy(),
                line_length=10,
                label_names=(
                    "Back edge",
                    "Front edge",
                    "DZ front edge",
                    "DZ back edge",
                ),
            )

            positions = annotated_positions.merge(
                positions, on="Time frame", how="outer"
            )
            positions = positions.dropna()
            positions["Full view x"] = positions["Window start"] + positions["x"]

            depth_positions = pd.read_csv(f"depth_data_{base_name}.csv", index_col=0)
            reslicing_positions = pd.read_csv(
                PROCESSED_DATA_DIR
                / f"spatio_temporal_reslicing/{base_name}_positions.csv",
                index_col=0,
            )
            depth_positions["Time frame"] = reslicing_positions["Time frame"]
            depth_positions["Feature"] = "Depth"
            depth_positions["Resliced frame"] = np.arange(depth_positions.shape[0])
            depth_ref = get_depth_ref(depth_positions, surface_mask)
            depth_ref["Feature"] = "MP depth ref"
            depth_positions = pd.concat((depth_positions, depth_ref))
            depth_positions = depth_positions.merge(
                reslicing_positions, on="Time frame"
            )
            positions = pd.concat((positions, depth_positions))

            try:
                depth_dz_positions = pd.read_csv(
                    f"depth_data_dz_{base_name}.csv", index_col=0
                )
                reslicing_positions = pd.read_csv(
                    PROCESSED_DATA_DIR
                    / f"spatio_temporal_reslicing/{base_name}_positions.csv",
                    index_col=0,
                )
                depth_dz_positions["Time frame"] = reslicing_positions["Time frame"]
                depth_dz_positions["Feature"] = "Depth DZ"
                depth_dz_positions["Resliced frame"] = np.arange(
                    depth_dz_positions.shape[0]
                )
                depth_dz_ref = get_depth_ref(depth_dz_positions, surface_mask)
                depth_dz_ref["Feature"] = "DZ depth ref"
                depth_dz_positions = pd.concat((depth_dz_positions, depth_dz_ref))
                depth_dz_positions = depth_dz_positions.merge(
                    reslicing_positions, on="Time frame"
                )
                positions = pd.concat((positions, depth_dz_positions))
            except FileNotFoundError:
                print(
                    f"Skipping dz depth because 'depth_data_dz_{base_name}.csv' does not exist."
                )

            positions["Full view x"] = positions["Window start"] + positions["x"]

            positions = positions.astype(
                {
                    "Full view x": int,
                    "x": int,
                    "y": int,
                    "Window start": int,
                    "Window stop": int,
                }
            )
            positions.to_csv(
                PROCESSED_DATA_DIR / f"positions/{path.stem}_positions.csv"
            )

            positions = positions.drop_duplicates(subset=["Time frame", "Feature"])

            positions = positions.replace("Front edge", "MP front edge")
            positions = positions.replace("Back edge", "MP back edge")
            positions = positions.replace("Depth", "MP deepest point")
            positions = positions.replace("Depth DZ", "DZ deepest point")

            positions["x"] = positions["Full view x"]
            positions = positions.pivot(
                index="Time frame", columns="Feature", values=["x", "y"]
            )
            positions.columns = positions.columns.to_flat_index()
            positions.columns = pd.Index(
                [feature + " " + coord for coord, feature in positions.columns]
            )

            for feature in ("MP", "DZ"):
                for point in ("front edge", "back edge", "deepest point", "depth ref"):
                    for coord in ("x", "y"):
                        col = f"{feature} {point} {coord}"
                        if col not in positions.columns:
                            positions[col] = np.nan

            positions = positions.sort_index()
            positions["MP width"] = (
                positions["MP back edge x"] - positions["MP front edge x"]
            )
            positions["DZ width"] = (
                positions["DZ back edge x"] - positions["DZ front edge x"]
            )
            positions["MP depth"] = (
                positions["MP deepest point y"] - positions["MP depth ref y"]
            )
            positions["DZ depth"] = (
                positions["DZ deepest point y"] - positions["DZ depth ref y"]
            )

            positions.to_csv(
                PROCESSED_DATA_DIR / f"positions/{path.stem}_positions.csv"
            )
